#!/usr/bin/env python
# -*- coding: utf-8 -*-
from bottle import route, run, template, get,post, request, static_file
import random
import string
import sqlite3
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import smtplib

# links zu chat gruppen oder websiten für die einzelnen gruppen...
links={
    "stupa":"link",
    "SoWi":"link",
    "PhilFak": "link",
    "Medizin":"link"
}

db = sqlite3.connect('data.db')
cur=db.cursor()
cur.execute("CREATE TABLE IF NOT EXISTS anfragen( name text, vorname text, fak text, frei text, bday text, nummer text, ort text, stra text, email text, tmpCheck text, stupa boolean, senat boolean, asp boolean, fsr text, fakra text)")
db.commit()

def sendMail(email, tmpCheck):
    s = smtplib.SMTP('mail.YouerServer')
    s.starttls()
    s.login('email@addresse.de', 'your password')
    msg=MIMEMultipart()
    message="Hallo, bitte klick auf den link, um deine Solikandidatur für die XXX zu bestätigen. <br> <a href='<base dir>/validate/"+tmpCheck+"'>validate</a>"
    msg['From']="noreply@yourServer"
    msg['To']=email
    msg['Subject']='Solikandidatur für die XXX'
    msg.attach(MIMEText(message,'html'))
    s.send_message(msg)
    del msg



@get('/')
@get('//')
def form():
    return open('form.html').read()

@post('/')
@post('//')
def save():
    paras=request.POST.decode()
    name=paras['name'].replace(","," ").replace(";"," ")#request.forms.get("name") #.decode('utf-8')
    vorname=paras['vorname'].replace(","," ").replace(";"," ")#request.forms.get('vorname')
    email=paras['email'].replace(","," ").replace(";"," ")#request.forms.get('email')
    if not(email.endswith("@stud.uni-goettingen.de")):
        return "das ist keine Uni addresse <a href='/soli/'> nochmal versuchen</a>"
    bday=paras['geburtsdatum'].replace(","," ").replace(";"," ")#request.forms.get('geburtsdatum')
    fak=paras['fakultaet'].replace(","," ").replace(";"," ")#request.forms.get('fakultaet')
    frei=paras['freiw'].replace(","," ").replace(";"," ")#request.forms.get('freiw')
    nummer=paras['matNa'].replace(","," ").replace(";"," ")#request.forms.get('matNa')
    ort=paras['plzOrt'].replace(","," ").replace(";"," ")#request.forms.get('plzOrt')
    stra=paras['str'].replace(","," ").replace(";"," ")#request.forms.get('str')
    print(paras.keys())
    stupa=False
    senat=False
    asp = False
    if( "stupa" in paras.keys()):
        stupa = True
    if( "senat" in paras.keys()):
        senat = True
    if( "isc" in paras.keys()):
        asp =True
    fsr = paras["FSR"]
    fakra=paras["FakRa"]

    tmpCheck=''.join(random.choice(string.ascii_lowercase) for i in range(50))
    cur.execute("INSERT INTO anfragen VALUES('"+name+"','"+vorname+"','"+fak+"','"+frei+"','"+bday+"','"+nummer+"','"+ort+"','"+stra+"','"+email+"','"+tmpCheck+"',"+str(stupa)+","+str(senat)+","+str(asp)+",'"+fsr+"','"+fakra+"')")
    db.commit()
    sendMail(email,tmpCheck)
    return "bitte check deine E-Mails"

@get('/validate/<check>')
def validate(check):
    anfr = cur.execute("SELECT name, vorname, fak, frei, bday, nummer, ort, stra,email FROM anfragen WHERE tmpCheck='"+check+"'").fetchone()
    places = cur.execute("SELECT stupa, senat, asp, fsr, fakra  FROM anfragen WHERE tmpCheck='"+check+"'").fetchone()
    if anfr==None:
        return 'validation Faild'
    erg=""
    for a in anfr:
        erg+=a+", "
    print(str(places[0]))
    linksTable="wenn du wissen willst wie es jetzt weiter geht kannst du in der/den gruppen der Listen Mitglied werden<br>"
    if(places[0]==1):
            with open('stupa.csv','a',encoding='UTF8') as file:
                file.write(erg+"\n")
            linksTable+="<a target='_blank' href='"+links['stupa']+"'>Stupa Gruppe</a><br>"
    if(places[1]==1):
            with open('senat.csv','a',encoding='UTF8') as file:
                file.write(erg+"\n")
    if(places[2]==1):
            with open('asp.csv','a',encoding='UTF8') as file:
                file.write(erg+"\n")
    if(places[3]!="keiner"):
            with open(places[3]+".csv",'a',encoding='UTF8') as file:
                file.write(erg+"\n")      
            linksTable+="<a target='_blank' href='"+links[places[3]]+"'>"+places[3]+" Gruppe</a>"
    if(places[4]!="keiner"):
            with open(places[4]+"_fakra.csv",'a',encoding='UTF8') as file:
                file.write(erg+"\n")
    cur.execute("DELETE FROM anfragen WHERE tmpCheck='"+check+"'")
    db.commit()

    return "Geschafft<br><br>"+linksTable

run(host='localhost',port=8080)
